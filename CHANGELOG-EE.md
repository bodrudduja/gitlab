# Changelog
- All notable changes to this project will be documented in this file.
- The format of adding new change: - [Developer's unique 3 digit name code] - JIRA issue number - Single line comment about change
- Start the comment with any of words (Added, Fixed, Removed, Changed) depending on the change context 
- This file is introduce by Bodrudduja

## [eGP BHUTAN - Second Phase]

## 2017-06-20
- [BDC] - EBDP-141 - Added a popup alert for TEC Members to see BOR before proceeding to declaration of COI
